Term Value provides a new compound field type allowing entry of term reference
and textfield pairs. The use case for this is to use a vocabulary to define
types of values that can be provided, leaving it up to the content author to
choose what values are provided.

Widgets
-------
Currently we only have a select list for term reference and textfield for
value. This module could potentially support autocomplete term reference and
textarea etc but not at the moment.

Formatters
----------
There are three formatters provided, choose which one suits you best. In future
additional settings may exist, such as linking the term reference to the term
page.

* Separate fields
  Attempts to render each term/value pair as if they were separate fields.
  Please report any issues that may arise with this formatter.

* Separate items
  Very basic rendering of each item using the Form API item type. See
  http://api.drupal.org/api/drupal/developer!topics!forms_api_reference.html/7#item

* Table of term/value pairs
  Terms on the left, values on the right. Some formatter settings could be
  useful here.

Views integration
-----------------
Works well for separate items and table formatters. Not so well for separate
fields. Not sure it will be possible to provide decent filtering options either.
